﻿angular.module('emails', ['ui.bootstrap']);
//angular.module('sharding');

var mainApplicationModuleName = 'SendEmailApplicationSample';
var mainApplicationModule = angular.module(mainApplicationModuleName
, ['ngRoute', 'ngResource', 'emails']);

mainApplicationModule.config(['$locationProvider',
    function ($locationProvider) {
        $locationProvider.hashPrefix('!');
    }
]);

//if (window.location.hash === '#_=_') window.location.hash = '#!';

angular.element(document).ready(function () {
    angular.bootstrap(document, [mainApplicationModuleName]);
});