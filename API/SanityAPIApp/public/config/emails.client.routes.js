﻿angular.module('emails').config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
    when('/', {
            templateUrl: './public/views/email/list.html'
        }).
    when('/emails', {
            templateUrl: './public/views/email/list.html'
        }).
    when('/emails/create', {
            templateUrl: './public/views/email/create.html'
        }).
    when('/emails/:emailId', {
            templateUrl: './public/views/email/view.html'
        }).
    when('/emails/:emailId/edit', {
            templateUrl: './public/views/email/edit.html'
        });
    }
]);