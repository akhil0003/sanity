﻿angular.module('emails').factory('EmailsService', ['$resource',
    function ($resource) {
        return $resource('api/emails/:emailId', {
            emailId: '@_id'
        }, {
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'DELETE'
            }
        });
}]);