﻿///<reference path="../../../../typings/express/express.d.ts"/>
/// <reference path="../../../../typings/inversify/inversify.d.ts" />

import express = require("express");
import EmailService = require("../../services/email/EmailService");
import IEmailService = require("../../services/email/IEmailService");
import EmailModel = require("../../models/email/EmailModel");
import IEmailController = require("./IEmailController");

class EmailController implements IEmailController {
    private _emailService: IEmailService;
    //private self;

    constructor(IEmailService: IEmailService) {
        this._emailService = IEmailService;
    }

    create(req: express.Request, res: express.Response): void {
        try {

            var email: EmailModel = <EmailModel>req.body;
            this._emailService.create(email, (error, result) => {
                if (error) res.send({ "error": "error" });
                else res.json(result);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });
        }
    }

    update(req: express.Request, res: express.Response): void {
        try {
            var email: EmailModel = <EmailModel>req.body;
            var _id: string = req.params._id;
            this._emailService.update(_id, email, (error, result) => {
                if (error) res.send({ "error": "error" });
                else res.json(email);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });

        }
    }

    delete(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;
            this._emailService.delete(_id, (error, result) => {
                if (error) res.send({ "error": "error" });
                else res.send({ "success": "success" });
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });

        }
    }

   
    retrieve(req: express.Request, res: express.Response): void {
        try {
            this._emailService.retrieve(req.query.page, (error, count, result) => {
                if (error) res.send({ "error": "error" });
                else {
                    res.json({
                        totalItems: count,
                        emails: result
                    });
                }
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });

        }
    }

    findById(req: express.Request, res: express.Response): void {
        try {

            var _id: string = req.params._id;
            this._emailService.findById(_id, (error, result) => {
                if (error) res.send({ "error": "error" });
                else res.send(result);
            });
        }
        catch (e) {
            console.log(e);
            res.send({ "error": "error in your request" });

        }
    }
}

export = EmailController;  