﻿import express = require("express");
import HomeController = require("../controllers/HomeController");
import IHomeController = require("../controllers/IHomeController");
import IHomeRoute = require("./IHomeRoute");

var router = express.Router();

class HomeRoute implements IHomeRoute {
    private _homeController: IHomeController;

    constructor(IHomeController: IHomeController) {
        this._homeController = IHomeController;
    }
    get routes() {
        var controller = this._homeController;
        router.get("/", controller.index.bind(controller));
        return router;
    }
}

Object.seal(HomeRoute);
export = HomeRoute;