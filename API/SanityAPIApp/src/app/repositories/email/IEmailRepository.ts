﻿import mongoose = require("mongoose");
import EmailModel = require("../../models/email/EmailModel");

interface IEmailRepository {
    create: (item: EmailModel, callback: (error: any, result: any) => void) => void;
    retrieve: (page: number, callback: (error: any, count: number, result: any) => void) => void;
    update: (_id: mongoose.Types.ObjectId, item: EmailModel, callback: (error: any, result: any) => void) => void;
    delete: (_id: string, callback: (error: any, result: any) => void) => void;
    findById: (_id: string, callback: (error: any, result: EmailModel) => void) => void;
}

export = IEmailRepository;