var DataAccess = require("../../repositories/DataContext");
var mongoose = DataAccess.mongooseInstance;
var mongooseConnection = DataAccess.mongooseConnection;
var EmailSchema = (function () {
    function EmailSchema() {
    }
    Object.defineProperty(EmailSchema, "schema", {
        get: function () {
            var schema = new mongoose.Schema({
                created: {
                    type: Date,
                    default: Date.now
                },
                subject: {
                    type: String,
                    trim: true,
                    required: 'Subject cannot be blank'
                },
                content: {
                    type: String,
                    default: ''
                },
                recipients: {
                    type: String
                }
            });
            schema.virtual('fullDetail').get(function () {
                return this.subject + ' ' + this.content;
            });
            schema.set('toJSON', { getters: true, virtuals: true });
            schema.set('toObject', { getters: true, virtuals: true });
            return schema;
        },
        enumerable: true,
        configurable: true
    });
    return EmailSchema;
})();
var schema = mongooseConnection.model('Email', EmailSchema.schema);
module.exports = schema;
//# sourceMappingURL=EmailSchema.js.map