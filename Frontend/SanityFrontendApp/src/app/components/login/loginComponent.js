var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('angular2/core');
var loginActions_1 = require("../../actions/loginActions");
var loginStore_1 = require("../../stores/loginStore");
var ng2_translate_1 = require('ng2-translate/ng2-translate');
var LoginComponent = (function () {
    function LoginComponent(loginActions, loginStore) {
        this.model = {};
        this.loginStore = loginStore;
        this.loginActions = loginActions;
    }
    LoginComponent.prototype.login = function (event) {
        this.loginActions.login(this.model);
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginStore.subscribe(function () {
            console.log("login success");
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login',
            providers: [loginActions_1.LoginActions, loginStore_1.LoginStore],
            templateUrl: './src/app/components/login/loginView.html',
            pipes: [ng2_translate_1.TranslatePipe]
        }),
        __param(0, core_1.Inject(loginActions_1.LoginActions)),
        __param(1, core_1.Inject(loginStore_1.LoginStore))
    ], LoginComponent);
    return LoginComponent;
})();
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=loginComponent.js.map